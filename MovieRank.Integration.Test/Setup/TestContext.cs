﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;

namespace MovieRank.Integration.Test.Setup
{
    class TestContext : IDisposable
    {
        private TestServer _server;

        public HttpClient Client {get;set;}

        public TestContext()
        {
            SetUpClient();
            RunCommandPromptCommand("docker pull amazon/dynamodb-local");
            RunCommandPromptCommand("docker run -d -p 8000:8000 amazon/dynamodb-local");
        }

        private void SetUpClient()
        {
            _server = new TestServer(new WebHostBuilder().UseEnvironment("Development").UseStartup<Startup>());
            _server.BaseAddress = new Uri("http://localhost:8000");
            Client = _server.CreateClient();
        }

        public static void RunCommandPromptCommand(string argument)
        {
            using (var process = new Process())
            {
                var startInfo = new ProcessStartInfo
                {
                    WindowsStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = $"/c {argument}"
                };
                process.StartInfo = startInfo;
                process.Start();
                process.WaitForExit();
            }
        }

        public void Dispose()
        {
            Client?.Dispose();
        }
    }
}
